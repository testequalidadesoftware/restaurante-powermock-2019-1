package br.ucsal.bes20191.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20191.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20191.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20191.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MesaDao.class, ComandaDao.class })
public class RestauranteBOUnitarioTest {

	/**
	 * Metodo a ser testado: public static Integer abrirComanda(Integer numeroMesa)
	 * throws RegistroNaoEncontrado, MesaOcupadaException. Verificar se a abertura
	 * de uma comanda para uma mesa livre apresenta sucesso. Lembre-se de verificar
	 * a chamada ao ComandaDao.incluir(comanda).
	 * @throws RegistroNaoEncontrado 
	 * @throws MesaOcupadaException 
	 */
	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = new Mesa(10);

		PowerMockito.mockStatic(MesaDao.class);
		PowerMockito.when(MesaDao.obterPorNumero(10)).thenReturn(mesa);
		
		PowerMockito.mockStatic(ComandaDao.class);
		
		RestauranteBO.abrirComanda(10);
		
		PowerMockito.verifyStatic();
		ComandaDao.incluir(Mockito.any(Comanda.class));

	}
}
